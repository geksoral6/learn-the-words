import {FETCH_CARDLIST, FETCH_CARDLIST_REJECT, FETCH_CARDLIST_RESOLVE} from "./actionTypes";

export const fetchCardList = (getData) => {
    return (dispatch, getState) => {
        dispatch(cardListAction());
        getData().on('value', res => {
            dispatch(cardListResolveAction(res.val()));
        });
    };
};

export const cardListAction = () => ({
    type: FETCH_CARDLIST
});

export const cardListResolveAction = (payload) => ({
    type: FETCH_CARDLIST_RESOLVE,
    payload,
});

export const cardListRejectAction = (err) => ({
    type: FETCH_CARDLIST_REJECT,
    err,
});
