import React, {Component} from 'react';
import { Button } from 'antd';
import FirebaseContext from "../../context/firebaseContext";

import s from './signOutButton.module.scss';

class SignOutBtn extends Component {

    clickHandler = () => {
        this.context.signOut();
        this.props.history.push('/login');
    };

    render() {
        return (
            <>
                <Button
                    type="dashed"
                    className={s.root}
                    onClick={ this.clickHandler }>Log out</Button>
            </>
        );
    }
}

SignOutBtn.contextType = FirebaseContext;

export default SignOutBtn;
