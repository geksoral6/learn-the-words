import React from 'react';
import cl from 'classnames';

import { CheckCircleOutlined, DeleteOutlined } from '@ant-design/icons';
import s from './Card.module.scss';

class Card extends React.Component {

    state = {
        done: false,
        isRemembered: false,
    };

    handleIsRememberClick = () => {
        if (!this.state.isRemembered) {
            this.setState((state) => {
                return {
                    done: !this.state.done,
                    isRemembered: !this.state.isRemembered
                }
            })
        }
    };

    handleDeletedClick = () => {
      this.props.onDeleted();
    };

    render() {
        const { done, isRemembered } = this.state;

        return (
            <div className={ cl(s.root) }>

                <div className={cl(s.card,
                    {[s.done]: done},
                    {[s.isRemembered]: isRemembered } )}
                     onClick={this.handleCardClick}>
                    <div className={s.cardInner}>
                        <div className={s.cardFront}>
                            {this.props.rus}
                        </div>

                        <div className={s.cardBack}>
                            {this.props.eng}
                        </div>
                    </div>
                </div>

                <div className={ cl(s.icons) }>
                    <CheckCircleOutlined className={ cl(s.goodBtn) }
                                         onClick={ this.handleIsRememberClick } />
                    <DeleteOutlined className={ cl(s.badBtn) }
                                    onClick={ this.handleDeletedClick }/>
                </div>
            </div>
        );
    }
}

export default Card;
