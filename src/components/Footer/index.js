import React from 'react';
import s from './footer.module.scss';

class Footer extends React.Component {

    render() {

        const { name, year } = this.props;

        return (
            <footer className={s.footer}>
                made by <span>{name}</span> at <span>{year}</span>
            </footer>
        );
    };
}

Footer.defaultProps = {name: "UserName", year: "2000"};

export default Footer;
