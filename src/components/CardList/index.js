import React from 'react';
import Card from '../Card';
import cl from 'classnames';
import {  Input } from 'antd';
import getTranslateWord from '../../services/yandex-dictionary';

import 'antd/dist/antd.css';
import s from './cardList.module.scss';



const { Search } = Input;

class CardList extends React.Component {

    state ={
        newEnglishWord: '',
        newRussianWord: '',
        isBusy: false,
    };

    handleInputChange = (e) => {
        this.setState({
            newEnglishWord: e.target.value,
        })
    };

    getTheWord = async () => {
        const tmpEngWord = this.state.newEnglishWord;
        const getWord = await getTranslateWord(tmpEngWord);
        const tmpRusWord = getWord[0].tr[0].text;

        this.props.onAddItem(tmpEngWord, tmpRusWord);

        this.setState({
            newEnglishWord: '',
            newRussianWord: '',
            isBusy: false,
        })
    };

    handleSubmitForm = async () => {
        this.setState({
           isBusy: true,
        }, this.getTheWord);
    };

    render() {
        const item = this.props.list;
        const value = this.state.newEnglishWord;
        const { isBusy } = this.state;
        return(
            <>
                <div className={cl(s.root)}>
                    {
                        item.map( ({eng, rus, id}) => (
                            <Card key={id}
                                  eng={eng}
                                  rus={rus}
                                  onDeleted={() => {
                                      this.props.onDeletedItem(id);
                                  }}/>
                        ))
                    }
                </div>

                <div className={ cl(s.panel) }>
                    <h2>Add new words...</h2>

                    <Search id='eng'
                            placeholder="input new russian word here..."
                            enterButton="Add"
                            size="large"
                            value={ value }
                            loading={ isBusy }
                            onSearch={ this.handleSubmitForm }
                            onChange={ this.handleInputChange }
                    />
                </div>
            </>
        )
    }
}

CardList.defaultProps ={
    item: [],
};

export default CardList;
