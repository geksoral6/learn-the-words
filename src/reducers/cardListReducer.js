import {FETCH_CARDLIST, FETCH_CARDLIST_REJECT, FETCH_CARDLIST_RESOLVE} from "../actions/actionTypes";

const cardListReducer = (state, action) => {
    switch (action.type) {
        case FETCH_CARDLIST:
            return {
                payload: [],
                err: null,
                isBusy: true,
            };
        case FETCH_CARDLIST_RESOLVE:
            return {
                payload: action.payload,
                err: null,
                isBusy: false,
            };
        case FETCH_CARDLIST_REJECT:
            return {
                payload: null,
                err: action.err,
                isBusy: false,
            };
        default:
            return {
                payload: null,
                err: null,
                isBusy: false,
        }
    }
};

export default cardListReducer;
