import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import thunk from 'redux-thunk';
import TestApp from './App';

import logger from 'redux-logger';

import FirebaseContext from './context/firebaseContext'
import Firebase from "./services/firebase";

import {createStore, applyMiddleware } from 'redux';
import rootReducer from './reducers';

const store = new createStore(rootReducer, applyMiddleware(thunk, logger));

ReactDOM.render(
    <Provider store={store}>
        <FirebaseContext.Provider value={new Firebase()}>

                <TestApp />

        </FirebaseContext.Provider>
    </Provider>
    , document.getElementById('root')
);
