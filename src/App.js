import React from 'react';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import { Spin } from 'antd';
import HomePage from './pages/Home';
import LoginPage from './pages/Login';
import {BrowserRouter, Switch, Route, Redirect} from 'react-router-dom';

import './App.css';
import './index.css';
import FirebaseContext from "./context/firebaseContext";
import {PrivateRoute} from "./utils/privateRoute";
import {addUserAction} from "./actions/userAction";



//
// export const wordsList = [
//     {
//         id: 1,
//         eng: 'between',
//         rus: 'между'
//     },
//     {
//         id: 2,
//         eng: 'high',
//         rus: 'высокий'
//     },
// ];

class TestApp extends React.Component {
    // state = {
    //     user: null,
    // };

    componentDidMount() {
        const { auth, setUserUid } = this.context;
        const { addUser } = this.props;

        auth.onAuthStateChanged((user) => {
            if (user) {
                setUserUid(user.uid);
                localStorage.setItem('user', JSON.stringify(user.uid));
                addUser(user.uid);

            } else {
                setUserUid(null);
                localStorage.removeItem('user');

            }
        })
    };

    render() {
        const  userUid  = this.props.userUid;
        console.log(userUid);
        if (userUid === null) {
            return (
                <div className='loader_wrap'>
                    <Spin />
                </div>
            );
        }

        return (
            <BrowserRouter>
                <Switch>
                    <PrivateRoute path='/' exact component={HomePage}/>
                    <Route path='/login' component={LoginPage}/>
                    <Redirect to='/'/>
                </Switch>

            </BrowserRouter>
        )
    }
}

TestApp.contextType = FirebaseContext;

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        addUser: addUserAction,
    }, dispatch);
};

const mapStateToProps = (state) => {
    return {
        userUid: state.user.userUid || null,
    }
};

export default connect(mapStateToProps,mapDispatchToProps)(TestApp);
