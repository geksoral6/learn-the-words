import React, {Component} from 'react';

import HeaderBlock from "../../components/HeaderBlock";
import Header from "../../components/Header";
import Paragraph from "../../components/Paragpaph";
import CardList from "../../components/CardList";
import Footer from "../../components/Footer";
import { Button } from 'antd';


import {withFirebase} from "../../context/firebaseContext";
import SignOutBtn from "../../components/SignOutButton";

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {cardListAction, cardListRejectAction, cardListResolveAction, fetchCardList} from "../../actions/cardListAction";


class HomePage extends Component {
    componentDidMount() {
        const { getUserCardsRef } = this.props.firebase;
        const { fetchCardList } = this.props;

        fetchCardList(getUserCardsRef);
    };

    setNewWord = (eng, rus) => {
        const { items } = this.props;

        const { getUserCardsRef } = this.props.firebase;
        const { fetchCardList } = this.props;

        const newWordArr = [
            ...items,
            {
                eng,
                rus,
                id: Math.floor(Math.random() * Math.floor(1000)),
            }
        ];

        //fetchCardList(getUserCardsRef().set(newWordArr));
        getUserCardsRef().set(newWordArr)
    };

    handleDeletedItem = (id) => {
        const { items } = this.props;

        const { getUserCardsRef } = this.props.firebase;

        const tmpWordArray = items.filter(item => item.id !== id);

        getUserCardsRef().set(tmpWordArray);
    };

    handleAddItem = (newEng, newRus) => {
        this.setNewWord(newEng, newRus);
    };

    render() {
        const { items } = this.props;
        return (
            <>
                <HeaderBlock>
                    <Header>
                        Time to learn words in Russian!
                    </Header>

                    <Paragraph>
                        Use flashcards to memorize and replenish vocabulary.
                    </Paragraph>
                </HeaderBlock>

                <HeaderBlock hideBackground>
                    <Header>What does it mean in English?</Header>
                    <CardList list={ items }
                              onDeletedItem={ this.handleDeletedItem }
                              onAddItem={ this.handleAddItem }/>
                </HeaderBlock>

                <SignOutBtn history={ this.props.history}/>

                <Footer name="Maxim S" year="2020"/>
            </>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        isBusy: state.cardList.isBusy,
        items: state.cardList.payload || [],
    };
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        fetchCardList: fetchCardList,
    }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(withFirebase(HomePage));
