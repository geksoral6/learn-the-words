import React, {Component} from 'react';

import { Form, Input, Button, Layout, Tabs } from 'antd';
import { UserOutlined, LockOutlined } from '@ant-design/icons';

import cl from 'classnames';

import s from './LoginPage.module.scss';
import {withFirebase} from "../../context/firebaseContext";


const { Content } = Layout;
const { TabPane } = Tabs;

class LoginPage extends Component {

    onLoginFinish = ({ email, password }) => {
        const { signWithEmail, setUserUid } = this.props.firebase;
        const { history } = this.props;
        signWithEmail(email, password)
            .then(res => {
                setUserUid(res.user.uid);
                localStorage.setItem('user', JSON.stringify(res.user.uid));
                history.push('/');
            })
    };

    onLoginFinishFailed = (errorMsg) => {
        console.log('##: error ', errorMsg);
    };

    renderLoginForm = () => {
        return (
            <Form
                name="normal_login"
                className={ cl(s.form_wrap) }
                initialValues={{
                    remember: true,
                }}
                onFinish={ this.onLoginFinish }
                onFinishFailed={ this.onLoginFinishFailed }
            >
                <h2>Login</h2>

                <Form.Item
                    name="email"
                    rules={[
                        {
                            required: true,
                            message: 'Please input your e-mail!',
                        },
                    ]}
                >
                    <Input prefix={<UserOutlined className="site-form-item-icon"/>} placeholder="E-mail"/>
                </Form.Item>

                <Form.Item
                    name="password"
                    rules={[
                        {
                            required: true,
                            message: 'Please input your Password!',
                        },
                    ]}
                >
                    <Input
                        prefix={<LockOutlined className="site-form-item-icon"/>}
                        type="password"
                        placeholder="Password"
                    />
                </Form.Item>

                <Form.Item>
                    <Button type="primary" htmlType="submit" className="login-form-button">
                        Log in
                    </Button>
                </Form.Item>
            </Form>

        )
    };

    onSigInFinish = ({ email, password }) => {
        const { createNewUserWithEmail } = this.props.firebase;
        const { history } = this.props;

        createNewUserWithEmail(email, password)
            .then(res => {
                console.log('####: res ', res);
                history.push('/');
            })
    };

    onSigInFinishFailed = (errorMsg) => {
        console.log('##: error ', errorMsg);
    };

    renderSignInForm = () => {
      return(
          <Form
              name="normal_login"
              className={ cl(s.form_wrap) }
              initialValues={{
                  remember: true,
              }}
              onFinish={ this.onSigInFinish }
              onFinishFailed={ this.onSigInFinishFailed }
          >
              <h2>Sign in now!</h2>

              <Form.Item
                  name="email"
                  rules={[
                      {
                          required: true,
                          message: 'Please input your e-mail!',
                      },
                  ]}
              >
                  <Input prefix={<UserOutlined className="site-form-item-icon"/>} placeholder="E-mail"/>
              </Form.Item>

              <Form.Item
                  name="password"
                  rules={[
                      {
                          required: true,
                          message: 'Please input your Password!',
                      },
                  ]}
              >
                  <Input
                      prefix={<LockOutlined className="site-form-item-icon"/>}
                      type="password"
                      placeholder="Password"
                  />
              </Form.Item>

              <Form.Item>
                  <Button type="primary" htmlType="submit" className="login-form-button">
                      Register now!
                  </Button>
              </Form.Item>
          </Form>
      )
    };

    render() {
        return (
            <Layout>
                <Content>
                    <div className={ cl(s.root) }>
                        <Tabs className={s.tab_wrapper} defaultActiveKey="1">
                            <TabPane  tab="Log in" key="1">
                                { this.renderLoginForm() }
                            </TabPane>
                            <TabPane tab="Sign in" key="2">
                                { this.renderSignInForm() }
                            </TabPane>
                        </Tabs>
                    </div>
                </Content>
            </Layout>
        );
    }
}


export default withFirebase(LoginPage);
